import axios from "axios";
import {
  ListDiscoverMoviesParams,
  ListDiscoverMoviesResponse,
  ListGenresRepsonse,
  ListPopularMoviesResponse,
  ListPopularPeopleResponse,
  SearchMoviesParams,
  SearchMoviesResponse,
} from "./types";

const movieApi = axios.create({
  baseURL: "https://api.themoviedb.org/3",
});

movieApi.interceptors.request.use((config) => {
  // Check if the URL already has query parameters
  const hasQueryParams = config.url!.includes("?");

  const apiKey = "0d372b5c962ca53a2b797cc84b049e10";
  // Append the API key
  config.url += hasQueryParams ? `&api_key=${apiKey}` : `?api_key=${apiKey}`;

  return config;
});

const objectToQueryString = (
  obj:
    | Record<string, string | number | boolean | null | undefined>
    | undefined
    | null,
) => {
  if (!obj) return "";

  return Object.keys(obj)
    .filter((key) => obj[key] !== null && obj[key] !== undefined)
    .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]!))
    .join("&");
};

export const searchMovies = async (params: SearchMoviesParams) => {
  const endpoint = "/search/movie";
  const qs = objectToQueryString(params);
  const url = `${endpoint}?${qs}`;

  const { data } = await movieApi.get<SearchMoviesResponse>(url);

  return data;
};

export const listMoviesGenres = async () => {
  const endpoint = "/genre/movie/list";

  const { data } = await movieApi.get<ListGenresRepsonse>(endpoint);

  return data;
};

export const listPopularMovies = async () => {
  const endpoint = "/movie/popular";

  const { data } = await movieApi.get<ListPopularMoviesResponse>(endpoint);

  return data;
};

export const listDiscoverMovies = async (params?: ListDiscoverMoviesParams) => {
  const endpoint = "/discover/movie";
  const qs = objectToQueryString(params);
  const url = `${endpoint}?${qs}`;

  const { data } = await movieApi.get<ListDiscoverMoviesResponse>(url);

  return data;
};

export const listPopularPeople = async () => {
  const endpoint = "/person/popular";

  const { data } = await movieApi.get<ListPopularPeopleResponse>(endpoint);

  return data;
};
