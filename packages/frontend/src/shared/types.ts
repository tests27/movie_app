export interface Movie {
  adult: boolean;
  backdrop_path?: string;
  genre_ids: number[];
  id: number;
  original_language: "en";
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface Genre {
  id: number;
  name: string;
}

export interface Person {
  adult: boolean;
  gender: number;
  id: number;
  known_for_department: string;
  name: string;
  original_name: string;
  popularity: number;
  profile_path: string;
  known_for: Movie[];
}

export type SearchMoviesParams = {
  query: string;
  page?: number;
  year?: string;
  include_adult?: boolean;
};

export interface SearchMoviesResponse {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
}

export interface ListGenresRepsonse {
  genres: Genre[];
}

export interface ListPopularMoviesResponse {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
}

export type ListDiscoverMoviesParams = {
  sort_by?:
  | "primary_release_date.desc"
  | "primary_release_date.asc"
  | "vote_average.desc";
  with_genres?: string;
};

export interface ListDiscoverMoviesResponse {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
}

export interface ListPopularPeopleResponse {
  page: number;
  results: Person[];
  total_pages: number;
  total_results: number;
}
