import { extendTheme } from "@chakra-ui/react";

export const theme = extendTheme({
  styles: {
    global: {
      html: {
        height: "100%",
      },
      body: {
        height: "100%",
        backgroundColor: "#1D1D29",
        color: "#DDDDDF",
      },
      "#root": {
        height: "100%",
      },
    },
  },
});
