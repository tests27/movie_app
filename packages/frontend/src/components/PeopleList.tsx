import { Stack } from "@chakra-ui/react";
import { Person } from "shared/types";
import PersonCard from "./PersonCard";

interface PeopleListProps {
  people: Person[];
}

const PeopleList: React.FC<PeopleListProps> = ({ people }) => {
  return (
    <Stack gap="16px">
      {people.map((person) => (
        <PersonCard key={person.id} person={person} />
      ))}
    </Stack>
  );
};

export default PeopleList;
