import { Stack } from "@chakra-ui/react";
import { MovieCard } from "components/MovieCard";
import { Genre, Movie } from "shared/types";

interface MovieListColumnProps {
  movies: Movie[];
  globalGenres: Genre[];
  cardsSize?: "sm" | "md";
}

const MovieListColumn: React.FC<MovieListColumnProps> = ({
  movies,
  globalGenres,
  cardsSize,
}) => {
  return (
    <Stack gap="20px">
      {movies.map((movie) => (
        <MovieCard
          key={movie.id}
          movie={movie as Movie}
          globalGenres={globalGenres}
          size={cardsSize}
          variant="listing"
        />
      ))}
    </Stack>
  );
};

export default MovieListColumn;
