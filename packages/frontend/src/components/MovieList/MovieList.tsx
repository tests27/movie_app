import { Genre, Movie } from "shared/types";
import MovieListRow from "./MovieListRow";
import MovieListColumn from "./MovieListColumn";

interface MovieListProps {
  movies: Movie[];
  globalGenres: Genre[];
  cardsSize?: "sm" | "md";
  direction?: "horizontal" | "vertical";
}

const MovieList: React.FC<MovieListProps> = ({
  movies,
  globalGenres,
  cardsSize,
  direction = "horizontal",
}) => {
  if (direction === "horizontal") {
    return (
      <MovieListRow
        movies={movies}
        globalGenres={globalGenres}
        cardsSize={cardsSize}
      />
    );
  } else {
    return (
      <MovieListColumn
        movies={movies}
        globalGenres={globalGenres}
        cardsSize={cardsSize}
      />
    );
  }
};

export default MovieList;
