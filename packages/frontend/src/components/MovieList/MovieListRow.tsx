import { Flex } from "@chakra-ui/react";
import { MovieCard } from "components/MovieCard";
import { useEffect, useRef } from "react";
import { Genre, Movie } from "shared/types";

interface MovieListRowProps {
  movies: Movie[];
  globalGenres: Genre[];
  cardsSize?: "sm" | "md";
}

const MovieListRow: React.FC<MovieListRowProps> = ({
  movies,
  globalGenres,
  cardsSize,
}) => {
  const scrollRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const onWheel = (event: WheelEvent) => {
      event.preventDefault();

      if (scrollRef.current) {
        scrollRef.current.scrollLeft += event.deltaY;
      }
    };

    const scrollElement = scrollRef.current;
    scrollElement?.addEventListener("wheel", onWheel);

    return () => scrollElement?.removeEventListener("wheel", onWheel);
  }, []);

  return (
    <Flex
      ref={scrollRef}
      overflowX="scroll"
      width="100%"
      gap="20px"
      paddingBottom="16px"
      sx={{
        "&::-webkit-scrollbar": {
          display: "none",
        },
        "-ms-overflow-style": "none" /* IE and Edge */,
        "scrollbar-width": "none" /* Firefox */,
        "&::before, &::after": {
          content: '""',
          display: "block",
          width: 0,
          height: 0,
        },
      }}
    >
      {movies.map((movie) => (
        <MovieCard
          key={movie.id}
          movie={movie as Movie}
          globalGenres={globalGenres}
          size={cardsSize}
          variant="banner"
        />
      ))}
    </Flex>
  );
};

export default MovieListRow;
