import { useMemo } from "react";
import { Genre, Movie } from "shared/types";
import MovieCardBannerSm from "./MovieCardBannerSm";
import MovieCardBannerMd from "./MovieCardBannerMd";
import MovieCardListingSm from "./MovieCardListingSm";

interface MovieCardProps {
  movie: Movie;
  globalGenres: Genre[];
  size?: "sm" | "md";
  variant?: "banner" | "listing";
}

const MovieCard: React.FC<MovieCardProps> = ({
  movie,
  globalGenres,
  size = "md",
  variant = "banner",
}) => {
  const genres = useMemo(() => {
    return globalGenres
      .filter((genre) => movie.genre_ids.includes(genre.id))
      .reduce((acc, genre, idx, arr) => {
        if (idx === arr.length - 1) {
          return acc + genre.name;
        }

        return acc + genre.name + ", ";
      }, "");
  }, [movie, globalGenres]);

  if (variant === "listing") {
    return <MovieCardListingSm movie={movie} />;
  } else {
    if (size === "sm") {
      return <MovieCardBannerSm movie={movie} />;
    } else {
      return <MovieCardBannerMd movie={movie} genres={genres} />;
    }
  }
};

export default MovieCard;
