import { Flex, Heading, Icon, Stack, Text } from "@chakra-ui/react";
import { FaStar } from "react-icons/fa6";
import { Movie } from "shared/types";

interface MovieCardBannerMdProps {
  movie: Movie;
  genres: string;
}

const MovieCardBannerMd: React.FC<MovieCardBannerMdProps> = ({
  movie,
  genres,
}) => {
  const imageSrc = movie.backdrop_path
    ? `https://image.tmdb.org/t/p/w780${movie.backdrop_path}`
    : `https://image.tmdb.org/t/p/w154${movie.poster_path}`;

  return (
    <Flex
      flex="0 0 auto"
      backgroundImage={imageSrc}
      backgroundSize="cover"
      backgroundPosition="center center"
      height="220px"
      width="440px"
      borderRadius="3xl"
      overflow="hidden"
    >
      <Flex
        padding="16px"
        width="100%"
        height="100%"
        background="linear-gradient(0deg, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0.00) 63.64%);"
        alignItems="flex-end"
      >
        <Stack color="white" gap="8px" width="100%">
          <Heading size="sm" textTransform="uppercase">
            {movie.title}
          </Heading>

          <Flex justifyContent="space-between">
            <Text>{genres}</Text>

            <Stack direction="row" alignItems="center" color="yellow.400">
              <Icon as={FaStar} />

              <Text>{movie.vote_average}</Text>
            </Stack>
          </Flex>
        </Stack>
      </Flex>
    </Flex>
  );
};

export default MovieCardBannerMd;
