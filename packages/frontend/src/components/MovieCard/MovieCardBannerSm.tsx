import { Flex } from "@chakra-ui/react";
import { Movie } from "shared/types";

interface MovieCardBannerSmProps {
  movie: Movie;
}

const MovieCardBannerSm: React.FC<MovieCardBannerSmProps> = ({ movie }) => {
  return (
    <Flex
      flex="0 0 auto"
      backgroundImage={`https://image.tmdb.org/t/p/w154${movie.poster_path}`}
      backgroundSize="cover"
      backgroundPosition="center center"
      height="220px"
      width="140px"
      borderRadius="3xl"
      overflow="hidden"
    />
  );
};

export default MovieCardBannerSm;
