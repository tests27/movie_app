import { Flex, Heading, Icon, Stack, Text } from "@chakra-ui/react";
import { useMemo } from "react";
import { FaHeart, FaStar } from "react-icons/fa";
import { Movie } from "shared/types";

interface MovieCardListingSmProps {
  movie: Movie;
}

const MovieCardListingSm: React.FC<MovieCardListingSmProps> = ({ movie }) => {
  const releaseYear = useMemo(
    () => new Date(movie.release_date).getFullYear(),
    [movie],
  );

  return (
    <Stack
      direction="row"
      width="100%"
      height="132px"
      borderRadius="3xl"
      overflow="hidden"
      background="#2A2C3D"
    >
      <Flex
        backgroundImage={`https://image.tmdb.org/t/p/w300${movie.backdrop_path}`}
        backgroundSize="cover"
        backgroundPosition="center center"
        overflow="hidden"
        width="40%"
        borderRadius="3xl"
      >
        <Flex
          padding="12px"
          flex="1 1"
          background="linear-gradient(0deg, rgba(0, 0, 0, 0.8) 0%, rgba(0, 0, 0, 0.00) 63.64%);"
          alignItems="flex-end"
        >
          <Flex gap="8px" flex="1 1">
            <Stack
              flex="1"
              gap="4px"
              direction="row"
              alignItems="center"
              color="red.400"
            >
              <Icon as={FaHeart} width="12px" height="12px" />
              <Text fontSize="12px" fontWeight="600">
                {Math.round(movie.popularity)}
              </Text>
            </Stack>

            <Stack
              flex="1"
              gap="4px"
              direction="row"
              alignItems="center"
              color="yellow.400"
            >
              <Icon width="12px" height="12px" as={FaStar} />
              <Text fontSize="12px" fontWeight="600">
                {movie.vote_average}
              </Text>
            </Stack>
          </Flex>
        </Flex>
      </Flex>

      <Stack flex="1" paddingY="16px" paddingX="12px" gap="8px">
        <Heading size="sm" noOfLines={1}>
          {movie.title} ({releaseYear})
        </Heading>

        <Text noOfLines={3}>{movie.overview}</Text>
      </Stack>
    </Stack>
  );
};

export default MovieCardListingSm;
