import {
  Button,
  Flex,
  Icon,
  Input,
  InputGroup,
  InputRightElement,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { MovieList } from "components/MovieList";
import { useEffect, useState } from "react";
import { FaSearch } from "react-icons/fa";
import { useQuery } from "react-query";
import { listMoviesGenres, searchMovies } from "shared/api";

const SidebarSearch = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [searchQuery, setSearchQuery] = useState("");
  const [searchYears, setSearchYears] = useState<number[]>([]);
  const [searchForKids, setSearchForKids] = useState(false);

  const genres = useQuery(["genres"], () => listMoviesGenres());

  const search = useQuery(
    ["search", searchQuery, searchYears, searchForKids],
    () =>
      searchMovies({
        query: searchQuery,
        year: searchYears.join(","),
        include_adult: !searchForKids,
      }),
    {
      enabled: false,
      onSuccess: () => onOpen(),
    },
  );

  useEffect(() => {
    if (searchQuery) {
      // debounce search
      const timeout = setTimeout(search.refetch, 500);

      return () => clearTimeout(timeout);
    }
  }, [searchQuery, searchYears, searchForKids]);

  const toggleYear = (year: number) => {
    setSearchYears((prev) => {
      if (prev.includes(year)) {
        return prev.filter((yearItem) => yearItem !== year);
      } else {
        return [...prev, year];
      }
    });
  };

  const toggleSearchForKids = () => {
    setSearchForKids((prev) => !prev);
  };

  return (
    <Popover
      returnFocusOnClose={false}
      isOpen={isOpen}
      onClose={onClose}
      placement="bottom"
      closeOnBlur={false}
    >
      <PopoverTrigger>
        <Stack paddingX="24px" gap="24px">
          <InputGroup size="lg">
            <Input
              placeholder="Search"
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
            />

            <InputRightElement pointerEvents="none">
              <Icon as={FaSearch} color="gray.300" />
            </InputRightElement>
          </InputGroup>

          <Flex gap="12px" flexWrap="wrap">
            <Button
              size="sm"
              onClick={() => toggleYear(2024)}
              colorScheme={searchYears.includes(2024) ? "teal" : "purple"}
            >
              New movies
            </Button>
            <Button
              size="sm"
              onClick={() => toggleYear(2023)}
              colorScheme={searchYears.includes(2023) ? "teal" : "purple"}
            >
              2023
            </Button>
            <Button
              size="sm"
              onClick={() => toggleYear(2022)}
              colorScheme={searchYears.includes(2022) ? "teal" : "purple"}
            >
              2022
            </Button>
            <Button
              size="sm"
              onClick={() => toggleYear(2021)}
              colorScheme={searchYears.includes(2021) ? "teal" : "purple"}
            >
              2021
            </Button>
            <Button
              size="sm"
              onClick={toggleSearchForKids}
              colorScheme={searchForKids ? "teal" : "purple"}
            >
              For kids
            </Button>
          </Flex>
        </Stack>
      </PopoverTrigger>

      <PopoverContent>
        <PopoverHeader color="gray.500" fontWeight="semibold">
          Results for {`"${searchQuery}"`}
        </PopoverHeader>
        <PopoverArrow />
        <PopoverCloseButton />
        <PopoverBody maxHeight="500px" overflowY="auto">
          {search.data && (
            <MovieList
              movies={search.data.results}
              globalGenres={genres.data?.genres || []}
              direction="vertical"
            />
          )}
        </PopoverBody>
        <PopoverFooter
          color="gray.500"
          display="flex"
          justifyContent="flex-end"
        >
          <Text>{search.data?.total_results} movies found</Text>
        </PopoverFooter>
      </PopoverContent>
    </Popover>
  );
};

export default SidebarSearch;
