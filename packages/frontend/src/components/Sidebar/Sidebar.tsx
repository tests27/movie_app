import { Box, Heading, Stack } from "@chakra-ui/react";
import SidebarProfile from "./SidebarProfile";
import SidebarSearch from "./SidebarSearch";
import { MovieList } from "components/MovieList";
import { useQuery } from "react-query";
import {
  listMoviesGenres,
  listPopularMovies,
  listPopularPeople,
} from "shared/api";
import PeopleList from "components/PeopleList";

const Sidebar = () => {
  const popularMovies = useQuery("popular-movies", () => listPopularMovies(), {
    // select only 2 items
    select: (data) => ({ ...data, results: data.results.slice(0, 2) }),
  });

  const genres = useQuery(["genres"], () => listMoviesGenres());

  const popularPeople = useQuery("popular-people", () => listPopularPeople(), {
    // select only 2 items
    select: (data) => ({ ...data, results: data.results.slice(0, 4) }),
  });

  return (
    <Stack width="400px" backgroundColor="#222432" gap="24px">
      <SidebarProfile />

      <SidebarSearch />

      <Stack gap="16px">
        <Box paddingX="24px">
          <Heading size="lg">Popular Film</Heading>
        </Box>

        <Box paddingX="24px">
          <MovieList
            movies={popularMovies.data?.results || []}
            globalGenres={genres.data?.genres || []}
            cardsSize="sm"
            direction="vertical"
          />
        </Box>
      </Stack>

      <Stack gap="16px">
        <Box paddingX="24px">
          <Heading size="lg">Popular Actors</Heading>
        </Box>

        <Box paddingX="24px">
          <PeopleList people={popularPeople.data?.results || []} />
        </Box>
      </Stack>
    </Stack>
  );
};

export default Sidebar;
