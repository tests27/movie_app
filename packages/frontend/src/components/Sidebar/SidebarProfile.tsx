import {
  Avatar,
  Button,
  ButtonGroup,
  Flex,
  IconButton,
  MenuDivider,
  Stack,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
} from "@chakra-ui/react";
import { FaChevronDown } from "react-icons/fa";
import { FaBell } from "react-icons/fa6";
import { FaLocationDot } from "react-icons/fa6";

const SidebarProfile = () => {
  return (
    <Flex padding="24px" justifyContent="space-between">
      <Stack gap="12px" direction="row">
        <Avatar size="md" name="Wade Warren" />

        <Menu>
          <MenuButton
            as={Button}
            rightIcon={<FaChevronDown />}
            variant="link"
            colorScheme="white"
          >
            Wade Warren
          </MenuButton>
          <MenuList>
            <MenuItem color="gray.500">Profile</MenuItem>
            <MenuItem color="gray.500">Subscription</MenuItem>
            <MenuItem color="gray.500">Saved movies</MenuItem>
            <MenuDivider />
            <MenuItem color="gray.500">Sign out</MenuItem>
          </MenuList>
        </Menu>
      </Stack>

      <ButtonGroup>
        <IconButton
          colorScheme="white"
          aria-label="Country"
          icon={<FaLocationDot size="20" />}
          variant="link"
          size="sm"
        />
        <IconButton
          colorScheme="white"
          aria-label="Notifications"
          icon={<FaBell size="20" />}
          variant="link"
          size="sm"
        />
      </ButtonGroup>
    </Flex>
  );
};

export default SidebarProfile;
