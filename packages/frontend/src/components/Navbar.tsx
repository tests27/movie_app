import { Box, Heading, Icon, Stack, Text } from "@chakra-ui/react";
import {
  FaHome,
  FaRegBookmark,
  FaRegClock,
  FaRegPlayCircle,
} from "react-icons/fa";
import { IoWalletOutline, IoSettingsOutline } from "react-icons/io5";
import { FiFileText } from "react-icons/fi";
import { FaRegEnvelope } from "react-icons/fa6";

const Navbar = () => {
  return (
    <Stack width="240px" backgroundColor="#222432" gap="36px">
      <Box padding="36px">
        <Heading size="lg">Mov.time</Heading>
      </Box>

      <Stack gap="48px">
        <Stack gap="12px">
          <Box paddingX="36px">
            <Heading size="sm">Menu</Heading>
          </Box>

          <Stack gap="8px">
            <Box
              color="blue.300"
              paddingX="33px"
              paddingY="16px"
              borderLeftWidth="3px"
              borderColor="blue.300"
              borderStyle="solid"
              backgroundColor="#2D2F3D"
            >
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={FaHome} />
                <Text>Home</Text>
              </Stack>
            </Box>

            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={FaRegBookmark} />
                <Text>Favorite</Text>
              </Stack>
            </Box>

            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={IoWalletOutline} />
                <Text>Purchase</Text>
              </Stack>
            </Box>

            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={FaRegClock} />
                <Text>Reminder</Text>
              </Stack>
            </Box>
          </Stack>
        </Stack>

        <Stack gap="12px">
          <Box paddingX="36px">
            <Heading size="sm">Other</Heading>
          </Box>

          <Stack gap="8px">
            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={FiFileText} />
                <Text>Playlist</Text>
              </Stack>
            </Box>

            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={FaRegPlayCircle} />
                <Text>Live</Text>
              </Stack>
            </Box>

            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={FaRegEnvelope} />
                <Text>Bookmarks</Text>
              </Stack>
            </Box>

            <Box color="gray.500" paddingX="36px" paddingY="16px">
              <Stack gap="8px" direction="row" alignItems="center">
                <Icon as={IoSettingsOutline} />
                <Text>Settings</Text>
              </Stack>
            </Box>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default Navbar;
