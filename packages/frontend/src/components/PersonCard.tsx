import { Avatar, Flex, Heading, Stack, Text } from "@chakra-ui/react";
import { useMemo } from "react";
import { Person } from "shared/types";

interface PersonCardProps {
  person: Person;
}

const PersonCard: React.FC<PersonCardProps> = ({ person }) => {
  const bestMovies = useMemo(
    () =>
      person.known_for
        .map((movie) => movie.title)
        .filter((title) => !!title)
        .join(", "),
    [person],
  );

  return (
    <Flex justifyContent="space-between" alignItems="center" gap="16px">
      <Stack direction="row" alignItems="center">
        <Avatar
          name={person.name}
          src={`https://image.tmdb.org/t/p/w45${person.profile_path}`}
        />

        <Stack gap="4px">
          <Heading size="sm">{person.name}</Heading>
          <Text noOfLines={1}>{bestMovies}</Text>
        </Stack>
      </Stack>

      <Stack gap="4px" textAlign="right">
        <Text>{person.popularity}</Text>
        <Text>Followers</Text>
      </Stack>
    </Flex>
  );
};

export default PersonCard;
