import { ChakraProvider } from "@chakra-ui/react";
import HomePage from "./pages/HomePage";
import { theme } from "./shared/theme";
import BaseLayout from "./pages/BaseLayout";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <ChakraProvider theme={theme}>
        <BaseLayout>
          <HomePage />
        </BaseLayout>
      </ChakraProvider>
    </QueryClientProvider>
  );
}

export default App;
