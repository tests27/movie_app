import { Box, Flex } from "@chakra-ui/react";
import Navbar from "components/Navbar";
import { Sidebar } from "components/Sidebar";
import { ReactNode } from "react";

interface BaseLayoutProps {
  children?: ReactNode;
}

const BaseLayout: React.FC<BaseLayoutProps> = ({ children }) => {
  return (
    <Flex height="100%">
      <Navbar />
      <Box as="main" flex="1" overflowX="hidden">
        {children}
      </Box>
      <Sidebar />
    </Flex>
  );
};

export default BaseLayout;
