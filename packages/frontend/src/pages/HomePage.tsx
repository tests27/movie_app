import { Box, Heading, Stack } from "@chakra-ui/react";
import { MovieList } from "components/MovieList";
import { useQuery } from "react-query";
import {
  listDiscoverMovies,
  listMoviesGenres,
  listPopularMovies,
} from "shared/api";

const HomePage = () => {
  const popularMovies = useQuery("popular-movies", () => listPopularMovies());
  const discoverMovies = useQuery("discover-movies", () =>
    listDiscoverMovies({ sort_by: "primary_release_date.desc" }),
  );
  const popularComedies = useQuery("comedy-movies", () =>
    listDiscoverMovies({ with_genres: "35,10751" }),
  );

  const genres = useQuery(["genres"], () => listMoviesGenres());

  return (
    <Stack gap="24px" paddingY="24px">
      <Stack gap="16px">
        <Box paddingX="24px">
          <Heading>Discovers</Heading>
        </Box>

        <MovieList
          movies={discoverMovies.data?.results || []}
          globalGenres={genres.data?.genres || []}
        />
      </Stack>

      <Stack gap="16px">
        <Box padding="24px">
          <Heading size="lg">Popular Film</Heading>
        </Box>

        <MovieList
          movies={popularMovies.data?.results || []}
          globalGenres={genres.data?.genres || []}
          cardsSize="sm"
        />
      </Stack>

      <Stack gap="16px">
        <Box padding="24px">
          <Heading size="lg">For Family Evening</Heading>
        </Box>

        <MovieList
          movies={popularComedies.data?.results || []}
          globalGenres={genres.data?.genres || []}
          cardsSize="sm"
        />
      </Stack>
    </Stack>
  );
};

export default HomePage;
