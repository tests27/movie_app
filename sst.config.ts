import { SSTConfig } from "sst";
import { WebStack } from "./stacks/WebStack";

export default {
  config(_input) {
    return {
      name: "movie-app",
      region: "eu-central-1",
    };
  },
  stacks(app) {
    app.stack(WebStack);
  },
} satisfies SSTConfig;
