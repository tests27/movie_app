import { StackContext, StaticSite } from "sst/constructs";

export function WebStack({ stack }: StackContext) {
  const site = new StaticSite(stack, "frontend", {
    path: "packages/frontend",
    buildOutput: "dist",
    buildCommand: "npm run build",
  });

  stack.addOutputs({
    SiteUrl: site.url,
  });
}
